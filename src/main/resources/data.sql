INSERT INTO users
(id, first_name, last_name, user_name, email, password, photo)
 VALUES
(1, 'Stas', 'Kosinskiy', 'skosinskiy', 'stanislav.kosinski@gmail.com', '$2a$10$LsVsLTHNDaJDu8dDbkGEk.4qDE8zIuiqvQ1Kvo99ET.gd.rqUQZjW', 'img/user/no-avatar.png');

INSERT INTO users
(id, first_name, last_name, user_name, email, password, photo)
 VALUES
(2, 'Leha', 'Drozdyuk', 'ldrozd', 'samyner@ukr.net', '$2a$10$LsVsLTHNDaJDu8dDbkGEk.4qDE8zIuiqvQ1Kvo99ET.gd.rqUQZjW', 'img\user\leha.jpg');

INSERT INTO users
    (id, first_name, last_name, user_name, email, password, photo)
 VALUES
(3, 'Zorik', 'Grigoryan', 'zgrigoryan', 'lyublyuShefa@mail.com', '$2a$10$LsVsLTHNDaJDu8dDbkGEk.4qDE8zIuiqvQ1Kvo99ET.gd.rqUQZjW', 'img/user/no-avatar.png');

insert into posts
    (id, date, text, photo, user_id)
values
    (1, '2018-08-20', 'testPost', 'img\post\1.jpeg', 2),
    (2, '2018-08-21', 'testPost', 'img\post\2.jpeg', 2),
    (3, '2018-08-22', 'testPost', 'img\post\3.jpeg', 2),
    (4, '2018-08-23', 'testPost', 'img\post\4.jpeg', 2),
    (5, '2018-08-24', 'testPost', 'img\post\1.jpeg', 1),
    (6, '2018-08-25', 'testPost', 'img\post\2.jpeg', 1),
    (7, '2018-08-26', 'testPost', 'img\post\3.jpeg', 1);


insert into followers
    (user, follower)
values
    (2, 1),
    (1, 2),
    (3, 2);

insert into likes
    (id, post_id, user_id)
values
    (1, 4, 1),
    (2, 4, 2);

insert into chats
    (id)
values
    (1);

insert into user_chats
    (chat_id, user_id)
values
    (1, 1),
    (1, 2);

insert into messages
    (id, text, chat_id, user_id)
values
    (1, 'Leha govnokoder', 1, 1),
    (2, 'Leha govnokoder2', 1, 2);

insert into comments
    (id, date, text, post_id, user_id)
values
    (1, '2018-08-20', 'first comment', 4, 2);

-- UPDATE HIBERNATE_SEQUENCES set SEQUENCE_NAME='default', NEXT_VAL=13
