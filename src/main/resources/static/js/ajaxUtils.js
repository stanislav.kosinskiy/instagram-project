async function getPrincipalUser() {
    let response = await fetch('/user', {
        credentials: 'same-origin'
    });
    return response.json();
}

async function ajaxJsonGET(url) {
    let response = await fetch(url, {
        credentials: 'same-origin'
    });
    return response.json();
}

async function ajaxJsonPOST(url, body) {
    let response = await fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        body: body
    });
    return response.json();
}