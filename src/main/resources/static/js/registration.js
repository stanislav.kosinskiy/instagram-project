const REGISTRATION_FORM_ID = 'registration-form';
const SUBMIT_BUTTON_ID = 'submit-registration';
const VALIDATION_URL = '/user/registration/validate';
const REGISTRATION_URL = '/user/registration';
const VALIDATION_TIMEOUT = 2000;

document.addEventListener('DOMContentLoaded', () => {

    let registrationForm = document.getElementById(REGISTRATION_FORM_ID);

    validateFieldsOnInput(registrationForm);
    validateFieldsOnFocusOut(registrationForm);
    registerUserOnSubmit()
});

async function registerUserOnSubmit() {
    document.getElementById(SUBMIT_BUTTON_ID).addEventListener('click', async event => {
        event.preventDefault();
        let formData = getFormData(REGISTRATION_FORM_ID);
        let validationResult = await ajaxJsonPOST(REGISTRATION_URL, formData);
        if (validationResult.status === 'successful') {
            window.location.replace('../registration-success.html');
        } else {
            checkAllFields(REGISTRATION_FORM_ID, VALIDATION_URL, SUBMIT_BUTTON_ID);
        }
    });
}

function validateFieldsOnFocusOut(registrationForm) {
    registrationForm.addEventListener('focusout', event => {
        checkField(event.target, REGISTRATION_FORM_ID, VALIDATION_URL, SUBMIT_BUTTON_ID);
        checkAllFields(REGISTRATION_FORM_ID, VALIDATION_URL, SUBMIT_BUTTON_ID);
    });
}

function validateFieldsOnInput(registrationForm) {
    registrationForm.addEventListener('input', event => {
        disableSubmitButton(SUBMIT_BUTTON_ID);
        setTimeout(checkField, VALIDATION_TIMEOUT, event.target, REGISTRATION_FORM_ID, VALIDATION_URL, SUBMIT_BUTTON_ID)
    });
}


