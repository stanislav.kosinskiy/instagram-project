document.addEventListener('DOMContentLoaded', async () => {
    let posts = await ajaxJsonGET('post/following');
    let principalUser = await getPrincipalUser();
    showPosts(posts, principalUser);

    document.getElementsByClassName('posts-container')[0].addEventListener('click', async event => {

        if (event.target.classList.contains('fa-heart') || event.target.classList.contains('post-modal-likes-add')) {
            let postBody = Array.from(event.path).find(element => element.classList.contains('modal-body'));
            let postId = getElementByClass(postBody, 'post-modal-id').textContent;
            processLike(event, postId, principalUser, postBody)
        }
    });

    document.getElementsByClassName('posts-container')[0].addEventListener('submit', async event => {
        let postBody = Array.from(event.path).find(element => element.classList.contains('modal-body'));
        if (event.target.classList.contains('post-comment-form')) {
            addComment(event, postBody);
        }
    })

});



function showPosts(posts, principalUser) {
    let body = document.getElementsByClassName('post-body')[0];
    let container = document.getElementsByClassName('posts-container')[0];
    posts.forEach(async post => {
        console.log(post);
        let bodyClone = body.cloneNode(true);
        removeClassFromElement(bodyClone, 'd-none');
        addOrRemoveLike(bodyClone, post.id, principalUser);
        setElementSrc(getElementByClass(bodyClone, 'post-user-photo'), post.user.photo);
        setElementText(getElementByClass(bodyClone, 'post-user-name'), post.user.userName);
        setElementSrc(getElementByClass(bodyClone, 'post-modal-image'), post.photo);
        setElementText(getElementByClass(bodyClone, 'post-modal-text'), post.text);
        setElementText(getElementByClass(bodyClone, 'post-modal-id'), post.id);
        displayPostComments(getElementByClass(bodyClone,'post-modal-comments'), post);
        container.append(bodyClone);

    });

}

async function addOrRemoveLike(bodyClone, postId, principalUser) {
    let likes = await ajaxJsonGET(`post/${postId}/likes`);
    let postModalHeartElement = getElementByClass(bodyClone,'post-modal-likes-add').firstElementChild;
    let hasLiked = likes.find(like => like.user.userName === principalUser.userName) !== undefined;
    if (!hasLiked) {
        setElementClass(postModalHeartElement, 'far fa-heart text-danger mr-2');
    } else {
        setElementClass(postModalHeartElement, 'fas fa-heart text-danger mr-2');
    }
    updateLikeCount(likes, bodyClone);
}

function updateLikeCount(likes, bodyClone) {
    getElementByClass(bodyClone, 'post-modal-likes').innerText = `${likes.length} like${likes.length===1?'':'s'}`;
}

function getElementByClass(parentElement, name) {
    return parentElement.getElementsByClassName(name)[0];
}

async function processLike(event, postId, principalUser, bodyClone) {
    event.preventDefault();
    let response = await fetch(`/post/${postId}/like`, {
        method: 'POST'
    });
    if (response.status === 200) {
        addOrRemoveLike(bodyClone, postId, principalUser);
    }
}

function displayPostComments(commentsElement, post) {
    commentsElement.innerHTML = '';
    post.comments.forEach(comment => {
        let commentDivElement = document.createElement('div');
        setElementClass(commentDivElement, 'row pl-3 pr-3');

        let commentSpanElement = document.createElement('span');

        commentDivElement.style.wordWrap = 'break-word';
        let commentUserElement = document.createElement('span');
        setElementClass(commentUserElement, 'mr-2');
        commentUserElement.innerText = comment.user.userName;

        let commentTextElement = document.createElement('span');
        setElementClass(commentTextElement, 'font-weight-light');
        commentTextElement.innerText = comment.text;

        commentSpanElement.append(commentUserElement);
        commentSpanElement.append(commentTextElement);

        commentDivElement.append(commentSpanElement);
        commentsElement.append(commentDivElement);
    });
}

async function addComment(event, bodyClone) {
    event.preventDefault();
    let postId = getElementByClass(bodyClone, 'post-modal-id').textContent;
    if (getElementByClass(bodyClone,'post-comment-input').value.trim().length > 0) {
        let formElement = getElementByClass(bodyClone, 'post-comment-form');
        let formData = new FormData(formElement);
        let response = await fetch(`/post/${postId}/comment`,{
            method: 'POST',
            body: formData
        });
        if (response.status === 200) {
            getElementByClass(bodyClone,'post-comment-input').value = '';
            let post = await ajaxJsonGET(`post/${postId}`);
            let commentsElement = getElementByClass(bodyClone,'post-modal-comments');
            displayPostComments(commentsElement, post);
        }
    }
}