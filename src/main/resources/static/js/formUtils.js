async function checkField(inputElement, formId, validationURL, submitButtonId) {
    let formData = getFormData(formId);
    let validationResult = await ajaxJsonPOST(validationURL, formData);
    let fieldErrors = validationResult.fieldErrors;
    if (inputElement.value.length > 0) {
        if (fieldErrors.hasOwnProperty(inputElement.name)) {
            removeClassFromElement(inputElement, 'border-success');
            showErrorMessage(inputElement, fieldErrors[inputElement.name]);
        } else {
            removeClassFromElement(inputElement, 'border-danger');
            removeErrorMessage(inputElement)
        }
    }
    activateSubmitButton(fieldErrors, submitButtonId);
}

function showErrorMessage(inputElement, errorMessage) {
    addClassToElement(inputElement, 'border-danger');
    if (inputElement.nextElementSibling === null) {
        appendErrorText(inputElement, errorMessage);
    } else {
        changeErrorText(inputElement, errorMessage);
    }
}

function removeErrorMessage(inputElement) {
    addClassToElement(inputElement, 'border-success');
    let errorMessageElement = inputElement.nextElementSibling;
    if(errorMessageElement !== null) {
        errorMessageElement.remove();
    }
}

function addClassToElement(element, classToAdd) {
    let currentElementClass = element.getAttribute('class');
    element.setAttribute('class', `${currentElementClass} ${classToAdd}`);
}

function removeClassFromElement(element, classToRemove) {
    let currentElementClass = element.getAttribute('class');
    element.setAttribute('class', currentElementClass.split(' ').filter(value => value !== classToRemove).join(' '));
}

function appendErrorText(inputElement, errorMessage) {
    let errorMessageElement = document.createElement('p');
    errorMessageElement.setAttribute('class', 'text-danger text-sm-left m-0');
    errorMessageElement.innerText = errorMessage;
    inputElement.parentNode.append(errorMessageElement);
}

function changeErrorText(inputElement, errorMessage) {
    let errorMessageElement = inputElement.nextElementSibling;
    errorMessageElement.setAttribute('class', 'text-danger text-sm-left m-0');
    errorMessageElement.innerText = errorMessage;
    inputElement.parentNode.append(errorMessageElement);
}

function activateSubmitButton(fieldErrors, submitButtonId) {
    if (Object.keys(fieldErrors).length === 0) {
        document.getElementById(submitButtonId).removeAttribute('disabled');
    }
}

function disableSubmitButton(submitButtonId) {
    document.getElementById(submitButtonId).disabled = true;
}

function getFormData(formId) {
    let formElement = document.getElementById(formId);
    return new FormData(formElement);
}

function checkAllFields(formId, validationUrl, submitButtonId) {
    let inputElements = Array.from(document.getElementById(formId).getElementsByTagName('input'));
    let filledInputElementsCount = inputElements.filter(input => input.value.length > 0).length;
    if (filledInputElementsCount === inputElements.length) {
        inputElements.forEach(input => checkField(input, formId, validationUrl, submitButtonId))
    }
}

function setElementClass(element, value) {
    element.setAttribute('class', value)
}

function setElementSrc(element, value) {
    element.setAttribute('src', value)
}

function setElementText(element, value) {
    element.innerText = value;
}

function hideElement(id) {
    getById(id).style.display = 'none';
}