document.addEventListener('DOMContentLoaded', async () => {
    let user = await getPrincipalUser();
    console.log(user);

    displayUserPage(user);
    addButtonActions(user);
    addFormValidations(user);
    addSearchActions(user);
});

function addSearchActions() {
    getById('navbar-search').addEventListener('input', async () => {
        let formData = getFormData('navbar-search-form');
        let users = await ajaxJsonPOST('user/search', formData);
        displaySearchList(users, 'followers');
    });

    getById('navbar-search').addEventListener('focusout', async () => {
        setTimeout(hideElement, 1000, 'navbar-search-body');
    });
}

function displayUserPage(user) {
    displayUserData(user);
    displayPostsBlock(user);
}

function addButtonActions(user) {
    let click = 'click';
    getById('edit-profile-button').addEventListener(click,() => fillUpdateForm(user));
    getById('followers-button').addEventListener(click,() => displayFollowList(user.followers, 'followers'));
    getById('following-button').addEventListener(click, () => displayFollowList(user.following, 'following'));
    getById('user-posts').addEventListener(click,event => fillPostModal(event, user));
    getById('submit-update-photo').addEventListener(click, event => updateUserPhoto(event));
    getById('submit-update-user').addEventListener(click, event => updateUser(event));
    getById('submit-create-post').addEventListener(click, event => createPost(event));
    getById('post-modal-likes-add').addEventListener(click, async event => processLike(event, user));
    getById('post-comment-form').addEventListener('submit', async event => addComment(event))
}

function addFormValidations() {
    let updatePhotoInputElement = getById('update-photo-input');
    let createPostPhotoInputElement = getById('create-post-photo');

    updatePhotoInputElement.addEventListener('change', () => {
        disableSubmitButton('submit-update-photo');
        checkField(updatePhotoInputElement, 'update-photo-form', 'user/update/photo/validate', 'submit-update-photo')
    });

    getById('edit-profile-form').addEventListener('input', event => {
        disableSubmitButton('submit-update-user');
        setTimeout(checkField, 2000, event.target, 'edit-profile-form','/user/update/validate', 'submit-update-user')
    });

    getById('edit-profile-form').addEventListener('focusout', event => {
        checkField(event.target, 'edit-profile-form','/user/update/validate', 'submit-update-user')
    });

    createPostPhotoInputElement.addEventListener('change', () => {
        disableSubmitButton('submit-create-post');
        checkField(createPostPhotoInputElement, 'create-post-form', 'user/createPost/validate', 'submit-create-post')
    });
}

function getById(id) {
    return document.getElementById(id)
}

async function processLike(event, user) {
    event.preventDefault();
    let postId = getById('post-modal-id').textContent;
    let response = await fetch(`/post/${postId}/like`, {
        method: 'POST'
    });
    if (response.status === 200) {
        addOrRemoveLike(postId, user);
    }
}

async function addOrRemoveLike(postId, user) {
    let likes = await ajaxJsonGET(`/post/${postId}/likes`);
    let postModalHeartElement = getById('post-modal-likes-add').firstElementChild;
    let hasLiked = likes.find(like => like.user.userName === user.userName) !== undefined;
    if (!hasLiked) {
        setElementClass(postModalHeartElement, 'far fa-heart text-danger mr-2');
    } else {
        setElementClass(postModalHeartElement, 'fas fa-heart text-danger mr-2');
    }
    updateLikeCount(likes);
}

function updateLikeCount(likes) {
    getById('post-modal-likes').innerText = `${likes.length} like${likes.length===1?'':'s'}`;
}

async function createPost(event) {
    event.preventDefault();
    let formData = getFormData('create-post-form');
    let validationResult = await ajaxJsonPOST('/user/createPost', formData);
    if (validationResult.status === 'successful') {
        window.location.reload(true)
    } else {
        checkAllFields('create-post-form', '/user/createPost/validate', 'submit-create-post');
    }
}

async function updateUser(event) {
    event.preventDefault();
    let formData = getFormData('edit-profile-form');
    let validationResult = await ajaxJsonPOST('/user/update', formData);
    if (validationResult.status === 'successful') {
        window.location.reload(true)
    } else {
        checkAllFields('edit-profile-form', '/user/update/validate', 'submit-update-user');
    }
}

async function updateUserPhoto(event) {
    event.preventDefault();
    let formData = getFormData('update-photo-form');
    let validationResult = await ajaxJsonPOST('/user/update/photo', formData);
    if (validationResult.status === 'successful') {
        window.location.reload(true)
    } else {
        let photoInputElement = getById('update-photo-input');
        let errorMessage = validationResult.fieldErrors['photo'];
        showErrorMessage(photoInputElement, errorMessage);
    }
}

async function fillPostModal(event, user) {
    let postId = getPostIdFromLink(event);
    let post = await ajaxJsonGET(`post/${postId}`);
    addOrRemoveLike(postId, user);
    setElementSrc(getById('post-user-photo'), user.photo);
    setElementText(getById('post-user-name'), user.userName);
    setElementSrc(getById('post-modal-image'), post.photo);
    setElementText(getById('post-modal-text'), post.text);
    displayPostComments(getById('post-modal-comments'), post);
    setElementText(getById('post-modal-id'), postId);
}

function getPostIdFromLink(event) {
    let postLink = event.path[1].href;
    return postLink.substring(postLink.search('/post/') + 6);
}

function displayUserData(user) {
    setElementSrc(getById('user-photo'), user.photo);
    setElementText(getById('user-login'), user.userName);
    setElementText(getById('user-name'), `${user.firstName} ${user.lastName}`);
    setElementText(getById('post-count'), user.posts.length);
    setElementText(getById('followers-count'), user.followers.length);
    setElementText(getById('following-count'), user.following.length);
}

// TODO refactor below functions

function displayPostsBlock(user) {
    let posts = user.posts;
    let count = 0;

    let userPostsElement = getById('user-posts');

    let postRowElement = document.createElement('div');
    postRowElement.setAttribute('class', 'row mb-4');
    userPostsElement.append(postRowElement);

    posts.sort((post1, post2) => new Date(post2.date) - new Date(post1.date));
    posts.forEach(post => {
        if (count%3 === 0) {
            postRowElement = document.createElement('div');
            postRowElement.setAttribute('class', 'row mb-4');
            userPostsElement.append(postRowElement);
        }
        count++;
        let imageLinkElement = document.createElement('a');
        imageLinkElement.setAttribute('class', 'col-4 post-modal-button');
        imageLinkElement.setAttribute('href', `/post/${post.id}`);
        imageLinkElement.setAttribute('data-toggle', 'modal');
        imageLinkElement.setAttribute('data-target', '#post-modal');

        let imageElement = document.createElement('img');
        imageElement.setAttribute('class', 'img-fluid post-image');
        imageElement.setAttribute('src', post.photo);
        imageLinkElement.append(imageElement);
        postRowElement.append(imageLinkElement);
    });
}

function displayFollowList(followList, type) {
    let followersBodyElement = getById(`${type}-modal-body`);
    let isFilled = followersBodyElement.children.length > 0;
    followList.forEach(follower => {
        if(!isFilled) {
            let followDivElement = document.createElement('a');
            followDivElement.setAttribute('class', 'mb-2 d-block text-dark');
            followDivElement.setAttribute('href', `/anotherUser.html?user/${follower.userName}`);

            let followImageElement = document.createElement('img');
            followImageElement.setAttribute('src', follower.photo);
            followImageElement.setAttribute('class', `${type}-modal-image img-fluid rounded-circle mr-3`);

            let followUsernameElement = document.createElement('span');
            followUsernameElement.innerText = follower.userName;

            followDivElement.append(followImageElement);
            followDivElement.append(followUsernameElement);
            followersBodyElement.append(followDivElement);
        }
    });
}

function displaySearchList(searchList, type) {
    let followersBodyElement = getById(`navbar-search-body`);
    followersBodyElement.style.display = 'block';
    followersBodyElement.innerHTML = '';
    searchList.forEach(user => {
        let followDivElement = document.createElement('a');
        followDivElement.setAttribute('class', 'mb-2 d-block text-dark');
        followDivElement.setAttribute('href', `/anotherUser.html?user/${user.userName}`);

        let followImageElement = document.createElement('img');
        followImageElement.setAttribute('src', user.photo);
        followImageElement.setAttribute('class', `${type}-modal-image img-fluid rounded-circle mr-3`);

        let followUsernameElement = document.createElement('span');
        followUsernameElement.innerText = user.userName;

        followDivElement.append(followImageElement);
        followDivElement.append(followUsernameElement);
        followersBodyElement.append(followDivElement);

    });
}

function fillUpdateForm(user) {
    let form = getById('edit-profile-form');
    Array.from(form).forEach(input => {
        if (input.type !== 'submit') {
            input.setAttribute('value', user[input.name])
        }
    });
}

async function addComment(event) {
    event.preventDefault();
    let postId = getById('post-modal-id').textContent;
    if (getById('post-comment-input').value.trim().length > 0) {
        let formData = getFormData('post-comment-form');
        let response = await fetch(`/post/${postId}/comment`,{
            method: 'POST',
            body: formData
        });
        if (response.status === 200) {
            getById('post-comment-form').getElementsByTagName('input')[0].value = '';
            let post = await ajaxJsonGET(`post/${postId}`);
            let commentsElement = getById('post-modal-comments');
            displayPostComments(commentsElement, post);
        }
    }
}

function displayPostComments(commentsElement, post) {
    commentsElement.innerHTML = '';
    post.comments.forEach(comment => {
        let commentDivElement = document.createElement('div');
        setElementClass(commentDivElement, 'row pl-3 pr-3');

        let commentSpanElement = document.createElement('span');

        commentDivElement.style.wordWrap = 'break-word';
        let commentUserElement = document.createElement('span');
        setElementClass(commentUserElement, 'mr-2');
        commentUserElement.innerText = comment.user.userName;

        let commentTextElement = document.createElement('span');
        setElementClass(commentTextElement, 'font-weight-light');
        commentTextElement.innerText = comment.text;

        commentSpanElement.append(commentUserElement);
        commentSpanElement.append(commentTextElement);

        commentDivElement.append(commentSpanElement);
        commentsElement.append(commentDivElement);
    });
}