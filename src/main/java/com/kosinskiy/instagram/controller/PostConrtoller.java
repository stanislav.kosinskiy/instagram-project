package com.kosinskiy.instagram.controller;

import com.kosinskiy.instagram.dto.PostDto;
import com.kosinskiy.instagram.entity.Post;
import com.kosinskiy.instagram.entity.Like;
import com.kosinskiy.instagram.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/post")
public class PostConrtoller {

	@Autowired
	private PostService postService;

	@GetMapping("/{postId}")
	public PostDto getPostDto(@PathVariable Long postId) {
		return postService.getPostDtoById(postId);
	}

	@PostMapping("/{postId}/like")
	public void addOrRemoveIfExistLike(Principal principal, @PathVariable Long postId) {
		postService.addOrRemoveIfExistLike(principal, postId);
	}

	@GetMapping("{postId}/likes")
	public List<Like> getPostLikes(@PathVariable Long postId) {
		return postService.getPostLikes(postId);
	}

	@PostMapping("/{postId}/comment")
	public void addComment(Principal principal, @PathVariable Long postId, @RequestParam String commentText) {
		postService.addComment(principal, postId, commentText);
	}

	@GetMapping("/following")
	public List<Post> getPostFromFollowing(Principal principal) {
		return postService.getPostsFromFollowing(principal);
	}
}
