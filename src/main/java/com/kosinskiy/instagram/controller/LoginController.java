package com.kosinskiy.instagram.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
public class LoginController {

	@RequestMapping("/")
	public String showUserPage() throws IOException {
		return "redirect:/user.html";
	}

	@RequestMapping("/login")
	public String showLoginForm() {
		return "login";
	}

	@RequestMapping("/registration")
	public String showRegistrationForm() {
		return "forward:/registration.html";
	}

}
