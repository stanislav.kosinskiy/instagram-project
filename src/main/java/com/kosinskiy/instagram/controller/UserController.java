package com.kosinskiy.instagram.controller;

import com.kosinskiy.instagram.dto.*;
import com.kosinskiy.instagram.entity.Post;
import com.kosinskiy.instagram.entity.User;
import com.kosinskiy.instagram.service.UserService;
import com.kosinskiy.instagram.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(path = "/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private ValidationService validationService;

	@GetMapping
	public UserDto getUserDtoFromPrincipal(Principal principal) {
		return userService.getUserDtoFromPrincipal(principal);
	}

	@GetMapping
	@RequestMapping("/{userName}")
	public UserDto getUserDtoByUserName(@PathVariable String userName) {
		return userService.getUserDtoByUserName(userName);
	}

	@PostMapping(path = "/update")
	public ValidationResultDto updateUser(HttpServletResponse response,
	                                      HttpServletRequest request,
	                                      Principal principal,
	                                      Authentication auth,
	                                      @Valid UserUpdateDto userUpdateDto, BindingResult bindingResult) {

		ValidationResultDto validationResultDto = validationService.getValidationResult(bindingResult);
		if (validationService.isValid(validationResultDto)) {
			userService.updatePrincipalUser(principal, userUpdateDto, auth, request, response);
		}
		return validationResultDto;

	}

	@PostMapping(path = "update/validate")
	public ValidationResultDto validateUpdateUser(@Valid UserUpdateDto userUpdateDto, BindingResult bindingResult) {
		return validationService.getValidationResult(bindingResult);
	}

	@PostMapping(path = "/update/photo")
	public ValidationResultDto updateUserPhoto(
			Principal principal,
			@Valid ImageDto imageDto,
			BindingResult bindingResult) throws IOException {

		ValidationResultDto validationResultDto = validationService.getValidationResult(bindingResult);
		if (validationService.isValid(validationResultDto)) {
			userService.updatePrincipalUserPhoto(principal, imageDto.getPhoto());
		}
		return validationResultDto;
	}

	@PostMapping(path = "/update/photo/validate")
	public ValidationResultDto validateUserPhoto(@Valid ImageDto imageDto, BindingResult bindingResult) {
		return validationService.getValidationResult(bindingResult);
	}

	@GetMapping(path = "/posts")
	public List<Post> getPosts(Principal principal) {
		return userService.getUserFromPrincipal(principal).getPosts();
	}

	@PostMapping(path = "/search")
	public List<User> searchFirstTenByUserNameStartingWith (@RequestParam String userName) {
		return userService.searchFirstTenByUserNameStartingWith(userName);
	}

	@GetMapping(path = "/subscribe/{userName}")
	public void subscribe(@PathVariable String userName, Principal principal) {
		userService.addOrRemoveIfExistFollowing(principal, userName);
	}

	@PostMapping(path = "/registration")
	public ValidationResultDto registerUser(@Valid UserRegistrationDto userRegistrationDto, BindingResult bindingResult) {
		ValidationResultDto validationResultDto = validationService.getValidationResult(bindingResult);
		if (validationService.isValid(validationResultDto)) {
			userService.createUser(userRegistrationDto);
		}
		return validationResultDto;
	}

	@PostMapping(path = "/registration/validate")
	public ValidationResultDto validateForm(@Valid UserRegistrationDto userRegistrationDto, BindingResult bindingResult) {
		return validationService.getValidationResult(bindingResult);
	}

	@PostMapping("/createPost")
	public ValidationResultDto createPost(HttpServletRequest request,
										  Principal principal,
										  @RequestParam String text,
										  @Valid ImageDto imageDto,
										  BindingResult bindingResult) throws IOException {

		ValidationResultDto validationResultDto = validationService.getValidationResult(bindingResult);
		if (validationService.isValid(validationResultDto)) {
			userService.addPost(text,  imageDto.getPhoto(), principal);
		}
		return validationResultDto;
	}

	@PostMapping("/createPost/validate")
	public ValidationResultDto validatePost(@Valid ImageDto imageDto, BindingResult bindingResult) {
		return validationService.getValidationResult(bindingResult);
	}
}
