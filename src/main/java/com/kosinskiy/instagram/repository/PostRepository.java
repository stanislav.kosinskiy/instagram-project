package com.kosinskiy.instagram.repository;

import com.kosinskiy.instagram.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

	@Query("from Post p where p.user.id=?1")
	List<Post> getPostsByUserId(Long userId);

	@Query("from Post p where p.user.id in ?1 order by p.date desc")
	List<Post> getPostsFromFollowing(List<Long> list);
}
