package com.kosinskiy.instagram.repository;

import com.kosinskiy.instagram.entity.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeRepository extends JpaRepository<Like, Long> {

	@Query("from Like l where l.post.id=?1")
	List<Like> getLikesByPostId(Long postId);

}
