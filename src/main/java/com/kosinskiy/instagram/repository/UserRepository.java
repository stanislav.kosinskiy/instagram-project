package com.kosinskiy.instagram.repository;

import com.kosinskiy.instagram.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query
	public User findByEmail(String email);

	@Query
	public User findByUserName(String userName);

	@Query
	public List<User> findAllByUserNameStartingWith(String userName);

}
