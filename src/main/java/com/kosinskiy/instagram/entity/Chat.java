package com.kosinskiy.instagram.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "chats")
@Data
public class Chat {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@ManyToMany
	@JoinTable(
			name = "user_chats",
			joinColumns = @JoinColumn (name ="chat_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id"))
	private List<User> users;

	@OneToMany(mappedBy = "chat")
	private List<Message> messages;
}
