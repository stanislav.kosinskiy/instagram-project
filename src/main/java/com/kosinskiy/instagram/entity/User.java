package com.kosinskiy.instagram.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kosinskiy.instagram.validation.EmailExtended;
import com.kosinskiy.instagram.validation.UniqueEmail;
import com.kosinskiy.instagram.validation.UniqueUserName;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	@Size(min = 2, message = "Too short first name")
	@Column(name = "first_name")
	private String firstName;
	@Size(min = 2, message = "Too short last name")
	@Column(name = "last_name")
	private String lastName;
	@EmailExtended
	@Column(name = "email")
	private String email;
	@Size(min = 2, message = "Too short login")
	@Column(name = "user_name")
	private String userName;
	@Size(min = 5, message = "Too short password")
	@Column(name = "password")
	@JsonIgnore
	private String password;
	@Column(name = "photo")
	private String photo;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Post> posts;

	@ManyToMany
	@JoinTable(
			name = "followers",
			joinColumns = @JoinColumn(name = "user"),
			inverseJoinColumns = @JoinColumn(name = "follower")
	)
	@JsonIgnore
	private List<User> followers;

	@ManyToMany
	@JoinTable(
			name = "followers",
			joinColumns = @JoinColumn(name = "follower"),
			inverseJoinColumns = @JoinColumn(name = "user")
	)
	@JsonIgnore
	private List<User> following;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "user_chats",
			joinColumns = @JoinColumn (name ="user_id"),
			inverseJoinColumns = @JoinColumn(name = "chat_id"))
	@JsonIgnore
	private List<Chat> chats;

}
