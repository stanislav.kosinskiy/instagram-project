package com.kosinskiy.instagram.validation;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class ValidImageConstraintValidator implements ConstraintValidator<ValidImage, MultipartFile> {

   private static final List<String> AllOWED_CONTENT_TYPES = Arrays.asList("image/png", "image/jpeg", "image/gif");

   public void initialize(ValidImage constraint) {
   }

   public boolean isValid(MultipartFile obj, ConstraintValidatorContext context) {
      return AllOWED_CONTENT_TYPES.contains(obj.getContentType());
   }
}
