package com.kosinskiy.instagram.validation;

import com.kosinskiy.instagram.entity.User;
import com.kosinskiy.instagram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueEmailConstraintValidator implements ConstraintValidator<UniqueEmail, String> {

	@Autowired
	private UserService userService;

	@Override
	public void initialize(UniqueEmail constraintAnnotation) {

	}

	@Override
	public boolean isValid(String email, ConstraintValidatorContext constraintValidatorContext) {
		return !isDuplicate(email);
	}

	private boolean isDuplicate(String email) {
		User userFromEmail = userService.findByEmail(email);
		boolean result = userFromEmail != null;
		if (UserService.isAuthenticated() && result) {
			result = !isCurrentEmail(userFromEmail);
		}
		return result;
	}

	private boolean isCurrentEmail(User userFromEmail) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User userFromSession = userService.findByUserName(userDetails.getUsername());
		return userFromSession.getEmail().equals(userFromEmail.getEmail());
	}
}
