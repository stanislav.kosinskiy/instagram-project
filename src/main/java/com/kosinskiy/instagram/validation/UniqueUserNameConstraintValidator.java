package com.kosinskiy.instagram.validation;

import com.kosinskiy.instagram.entity.User;
import com.kosinskiy.instagram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UniqueUserNameConstraintValidator implements ConstraintValidator<UniqueUserName, String> {

	@Autowired
	private UserService userService;

	@Override
	public void initialize(UniqueUserName constraintAnnotation) {

	}

	@Override
	public boolean isValid(String userName, ConstraintValidatorContext constraintValidatorContext) {
		return !isDuplicate(userName);
	}

	private boolean isDuplicate(String userName) {
		User userFromUserName = userService.findByUserName(userName);
		boolean result = userFromUserName != null;
		if (UserService.isAuthenticated() && result) {
			result = !isCurrentUserName(userFromUserName);
		}
		return result;
	}

	private boolean isCurrentUserName(User userFromUserName) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails.getUsername().equals(userFromUserName.getUserName());
	}
}
