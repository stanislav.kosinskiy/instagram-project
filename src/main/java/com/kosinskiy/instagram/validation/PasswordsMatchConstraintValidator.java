package com.kosinskiy.instagram.validation;

import com.kosinskiy.instagram.dto.UserRegistrationDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordsMatchConstraintValidator implements ConstraintValidator<PasswordsMatch, UserRegistrationDto> {
	@Override
	public void initialize(PasswordsMatch constraintAnnotation) {

	}

	@Override
	public boolean isValid(UserRegistrationDto userRegistrationDto, ConstraintValidatorContext constraintValidatorContext) {
		return userRegistrationDto.getPassword().equals(userRegistrationDto.getPasswordConfirmation());
	}
}
