package com.kosinskiy.instagram.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDto {

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String photo;
	private List<PostDto> posts;
	private List<UserShortDto> followers;
	private List<UserShortDto> following;

}
