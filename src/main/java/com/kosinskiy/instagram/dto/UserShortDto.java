package com.kosinskiy.instagram.dto;

import lombok.Data;

@Data
public class UserShortDto {

	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String photo;

}
