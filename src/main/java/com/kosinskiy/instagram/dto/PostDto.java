package com.kosinskiy.instagram.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class PostDto {

	private long id;
	private Date date;
	private String text;
	private String photo;
	private UserShortDto user;
	private List<LikeShortDto> likes;
	private List<CommentShortDto> comments;
}
