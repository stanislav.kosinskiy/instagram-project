package com.kosinskiy.instagram.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CommentShortDto {

	private int id;
	private Date date;
	private String text;
	private UserShortDto user;

}
