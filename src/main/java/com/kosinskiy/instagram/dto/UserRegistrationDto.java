package com.kosinskiy.instagram.dto;

import com.kosinskiy.instagram.validation.EmailExtended;
import com.kosinskiy.instagram.validation.PasswordsMatch;
import com.kosinskiy.instagram.validation.UniqueEmail;
import com.kosinskiy.instagram.validation.UniqueUserName;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@PasswordsMatch
public class UserRegistrationDto {
	@Size(min = 2, message = "Too short first name")
	private String firstName;
	@Size(min = 2, message = "Too short last name")
	private String lastName;
	@EmailExtended
	@UniqueEmail
	private String email;
	@Size(min = 2, message = "Too short login")
	@UniqueUserName
	private String userName;
	@Size(min = 5, message = "Too short password")
	private String password;
	@Size(min = 5, message = "Too short password")
	private String passwordConfirmation;
}
