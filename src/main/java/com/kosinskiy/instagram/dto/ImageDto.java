package com.kosinskiy.instagram.dto;

import com.kosinskiy.instagram.validation.ValidImage;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ImageDto {

	@ValidImage
	private MultipartFile photo;
}
