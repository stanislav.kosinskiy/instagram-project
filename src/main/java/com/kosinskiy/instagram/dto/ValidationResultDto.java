package com.kosinskiy.instagram.dto;

import lombok.Data;

import java.util.HashMap;

@Data
public class ValidationResultDto {

	private String status;

	private HashMap<String, String> fieldErrors = new HashMap<>();

}
