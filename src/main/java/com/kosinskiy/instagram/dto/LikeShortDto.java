package com.kosinskiy.instagram.dto;

import lombok.Data;

@Data
public class LikeShortDto {

	private long id;
	private UserShortDto user;

}
