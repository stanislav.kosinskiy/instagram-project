package com.kosinskiy.instagram.security;

import com.kosinskiy.instagram.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.headers()
				.frameOptions()
					.disable()
					.and()
				.csrf()
				.disable()
				.authorizeRequests()
					.antMatchers("/", "/registration-success.html", "/user/registration", "/user/registration/**", "/css/**", "/js/**", "/img/**", "/registration", "/h2-console/**")
					.permitAll()
					.anyRequest().authenticated()
					.and()
				.formLogin()
					.loginPage("/login")
					.successForwardUrl("/")
					.permitAll()
					.and()
				.logout()
					.permitAll()
					.and()
				.rememberMe().key("uniqueKey").tokenValiditySeconds(86400);
	}

	@Override
	public UserDetailsService userDetailsService() {
		return userService;
	}
}
