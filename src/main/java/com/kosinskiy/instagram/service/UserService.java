package com.kosinskiy.instagram.service;

import com.kosinskiy.instagram.dto.UserDto;
import com.kosinskiy.instagram.dto.UserRegistrationDto;
import com.kosinskiy.instagram.dto.UserUpdateDto;
import com.kosinskiy.instagram.entity.Post;
import com.kosinskiy.instagram.entity.User;
import com.kosinskiy.instagram.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Value("${image.post.width}")
	private int IMAGE_WIDTH;
	@Value("${image.post.height}")
	private int IMAGE_HEIGHT;
	@Value("${image.post.path}")
	private String IMAGE_PATH;

	private static final String PERSISTENT_COOKIE_NAME = "remember-me";
	private static final String IMAGE_EXTENSION = ".jpg";
	private static final String IMAGE_FORMAT = IMAGE_EXTENSION.substring(1);
	private static final String DEFAULT_USER_PHOTO = "img/user/no-avatar.png";

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public User findByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}

	public UserDto getUserDtoByUserName(String userName) {
		return modelMapper.map(userRepository.findByUserName(userName), UserDto.class);
	}

	public User getUserFromPrincipal(Principal principal) {
		return findByUserName(principal.getName());
	}

	public UserDto getUserDtoFromPrincipal(Principal principal) {
		return modelMapper.map(getUserFromPrincipal(principal), UserDto.class);
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = findByUserName(userName);
		return org.springframework.security.core.userdetails.User.builder()
				.username(user.getUserName())
				.roles("USER")
				.password(user.getPassword())
				.build();
	}

	public void createUser(@Valid UserRegistrationDto userRegistrationDto) {
		User user = User.builder()
				.firstName(userRegistrationDto.getFirstName())
				.lastName(userRegistrationDto.getLastName())
				.userName(userRegistrationDto.getUserName())
				.email(userRegistrationDto.getEmail())
				.password(passwordEncoder.encode(userRegistrationDto.getPassword()))
				.photo(DEFAULT_USER_PHOTO)
				.build();
		userRepository.save(user);
	}

	public void updatePrincipalUser(Principal principal, UserUpdateDto userUpdateDto, Authentication auth, HttpServletRequest request, HttpServletResponse response) {
		User principalUser = getUserFromPrincipal(principal);
		boolean isUsernameChanged = hasUsernameChanged(principalUser, userUpdateDto);
		updateUserData(principalUser, userUpdateDto);
		userRepository.save(principalUser);
		if (isUsernameChanged) {
			updateAuthentication(request, response, userUpdateDto, auth);
		}
	}

	private boolean hasUsernameChanged(User user, UserUpdateDto userUpdateDto) {
		return !user.getUserName().equals(userUpdateDto.getUserName());
	}

	private void updateUserData(User user, UserUpdateDto userUpdateDto) {
		user.setFirstName(userUpdateDto.getFirstName());
		user.setLastName(userUpdateDto.getLastName());
		user.setUserName(userUpdateDto.getUserName());
		user.setEmail(userUpdateDto.getEmail());
	}

	private void updateAuthentication(HttpServletRequest request, HttpServletResponse response, UserUpdateDto userUpdateDto, Authentication auth) {
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(loadUserByUsername(userUpdateDto.getUserName()), auth.getCredentials(), auth.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(authToken);
		cancelRememberMeCookie(request, response);
	}

	private void cancelRememberMeCookie(HttpServletRequest request, HttpServletResponse response) {
		Cookie cookie = new Cookie(PERSISTENT_COOKIE_NAME, null);
		cookie.setMaxAge(0);
		cookie.setPath(StringUtils.hasLength(request.getContextPath()) ? request.getContextPath() : "/");
		response.addCookie(cookie);
	}

	public void updatePrincipalUserPhoto(Principal principal, MultipartFile photo) throws IOException {
		User principalUser = getUserFromPrincipal(principal);
		String fileName = saveAndResizeImage(photo, principalUser.getUserName());
		principalUser.setPhoto(fileName);
		userRepository.save(principalUser);
	}

	private String saveAndResizeImage(MultipartFile photo, String userName) throws IOException {
		String fileName = UUID.randomUUID().toString() + IMAGE_EXTENSION;
		File imageDirPath = Paths.get(IMAGE_PATH, userName).toFile();
		imageDirPath.mkdirs();
		File imagePath = new File(imageDirPath, fileName);
		photo.transferTo(imagePath);
		resizeImage(imagePath);
		return new File(userName, fileName).toString();
	}
	//TODO make responsive resizing
	private void resizeImage(File originalImage) throws IOException {
		BufferedImage inputImage = ImageIO.read(originalImage);
		BufferedImage outputImage = new BufferedImage(IMAGE_WIDTH, IMAGE_HEIGHT, inputImage.getType());
		Graphics2D g2d = outputImage.createGraphics();
		g2d.drawImage(inputImage, 0, 0, IMAGE_WIDTH, IMAGE_HEIGHT, null);
		g2d.dispose();
		ImageIO.write(outputImage, IMAGE_FORMAT, originalImage);
	}

	public List<User> searchFirstTenByUserNameStartingWith(String userName) {
		return userRepository.findAllByUserNameStartingWith(userName);
	}

	public void addOrRemoveIfExistFollowing(Principal principal, String userName) {
		User principalUser = getUserFromPrincipal(principal);
		User user = findByUserName(userName);
		Optional<User> principalFollowing = principalUser.getFollowing()
				.stream()
				.filter(following -> following.getUserName().equals(userName))
				.findFirst();
		addOrRemoveFollowing(principalFollowing, principalUser, user);
		userRepository.save(principalUser);
	}

	private void addOrRemoveFollowing(Optional<User> principalFollowing, User principalUser, User user) {
		if (principalFollowing.isPresent()) {
			principalUser.getFollowing().remove(user);
		} else {
			principalUser.getFollowing().add(user);
		}
	}

	public static boolean isAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails;
	}

	public void addPost(String postText, MultipartFile photo, Principal principal) throws IOException {
		User principalUser = getUserFromPrincipal(principal);
		String fileName = saveAndResizeImage(photo, principalUser.getUserName());
		principalUser.getPosts().add(Post.builder()
				.date(new Date())
				.text(postText)
				.photo(fileName)
				.user(principalUser)
				.build());
		userRepository.save(principalUser);
	}
}
