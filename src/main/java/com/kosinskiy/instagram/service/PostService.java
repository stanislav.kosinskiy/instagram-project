package com.kosinskiy.instagram.service;

import com.kosinskiy.instagram.dto.PostDto;
import com.kosinskiy.instagram.entity.Comment;
import com.kosinskiy.instagram.entity.Like;
import com.kosinskiy.instagram.entity.Post;
import com.kosinskiy.instagram.entity.User;
import com.kosinskiy.instagram.repository.PostRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PostService {

	@Autowired
	private UserService userService;
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private ModelMapper modelMapper;

	public List<Post> getPosts() {
		return postRepository.findAll();
	}

	private Post getPost(Long id) {
		return postRepository.findById(id).orElse(Post.builder().build());
	}

	public PostDto getPostDtoById(Long id) {
		return modelMapper.map(getPost(id), PostDto.class);
	}

	public void addOrRemoveIfExistLike(Principal principal, Long postId) {
		User principalUser = userService.getUserFromPrincipal(principal);
		Post post = getPost(postId);
		Optional<Like> principalLike = post.getLikes()
				.stream()
				.filter(like -> like.getUser().getId() == principalUser.getId())
				.findFirst();
		addOrRemoveLike(principalLike, post, principalUser);
		postRepository.save(post);
	}

	private void addOrRemoveLike(Optional<Like> principalLike, Post post, User user) {
		if (principalLike.isPresent()) {
			post.getLikes().remove(principalLike.get());
		} else {
			post.getLikes().add(Like.builder()
									.user(user)
									.post(post)
									.build());
		}
	}

	public void addComment(Principal principal, Long postId, String commentText) {
		User principalUser = userService.getUserFromPrincipal(principal);
		Post post = getPost(postId);
		post.getComments().add(Comment.builder()
								.date(new Date())
								.text(commentText)
								.user(principalUser)
								.post(post)
								.build());
		postRepository.save(post);
	}

	public List<Like> getPostLikes(Long postId) {
		Optional<Post> optionalPost = postRepository.findById(postId);
		return optionalPost.isPresent() ? optionalPost.get().getLikes() : new ArrayList<>();
	}

	public List<Post> getPostsFromFollowing(Principal principal) {
		User principalUser = userService.getUserFromPrincipal(principal);
		List<Long> followingIdList = principalUser
				.getFollowing()
				.stream()
				.map(User::getId)
				.collect(Collectors.toList());
		return postRepository.getPostsFromFollowing(followingIdList);
	}
}
