package com.kosinskiy.instagram.service;

import com.kosinskiy.instagram.entity.Like;
import com.kosinskiy.instagram.repository.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LikeService {

	@Autowired
	private LikeRepository likeRepository;

	public List<Like> getLikesByPostId(Long postId) {
		return likeRepository.getLikesByPostId(postId);
	}
}
