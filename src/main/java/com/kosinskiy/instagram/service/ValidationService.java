package com.kosinskiy.instagram.service;

import com.kosinskiy.instagram.dto.ValidationResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

@Service
public class ValidationService {

	@Autowired
	private MessageSource messageSource;

	private static final String SUCCESSFUL_VALIDATION_STATUS = "successful";
	private static final String FAILED_VALIDATION_STATUS = "failed";

	public ValidationResultDto getValidationResult(BindingResult bindingResult) {
		ValidationResultDto result = new ValidationResultDto();
		if (bindingResult.hasErrors()) {
			result.setStatus(FAILED_VALIDATION_STATUS);
			for (Object object : bindingResult.getAllErrors()) {
				if(object instanceof FieldError) {
					FieldError fieldError = (FieldError) object;
					result.getFieldErrors().put(fieldError.getField(), messageSource.getMessage(fieldError, null));
				} else if (object instanceof ObjectError) {
					ObjectError objectError = (ObjectError) object;
					result.getFieldErrors().put("passwordConfirmation", messageSource.getMessage(objectError, null));
				}
			}
		} else {
			result.setStatus(SUCCESSFUL_VALIDATION_STATUS);
		}
		return result;
	}

	public boolean isValid(ValidationResultDto validationResultDto) {
		return SUCCESSFUL_VALIDATION_STATUS.equals(validationResultDto.getStatus());
	}

}
